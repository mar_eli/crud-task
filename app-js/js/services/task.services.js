const API = "http://localhost:4000/api/tasks";

//Listamos todas las tareas de la api
export const list = async () => {
    try {
        const response = await fetch(API);
        const data = await response.json();
        return data;
    } catch (error) {
        return { status: false, message: error.message };
    }
};

//Obtenemos una tarea de la api
export const get = async (id) => {
    try {
        const response = await fetch(`${API}/${id}`);
        const data = await response.json();
        return data;
    } catch (error) {
        return { status: false, message: error.message };
    }
};

//Creamos una tarea
export const create = async (task) => {
    try {
        const response = await fetch(API, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(task),
        });
        const data = await response.json();
        return data;
    } catch (error) {
        return { status: false, message: error.message };
    }
};

//Editamos una tarea
export const update = async (id, task) => {
    try {
        const response = await fetch(`${API}/${id}`, {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(task),
        });
        const data = await response.json();
        return data;
    } catch (error) {
        return { status: false, message: error.message };
    }
};

//Eliminamos una tarea de la api
export const deleteData = async (id) => {
    try {
        const response = await fetch(`${API}/${id}`, {
            method: "DELETE",
        });

        const data = await response.json();
        return data;
    } catch (error) {
        return { status: false, message: error.message };
    }
};