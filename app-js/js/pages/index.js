import { TaskItem } from "../components/TaskItem.js";
import { list, deleteData } from "../services/task.services.js"

//Dom elements
const taskList = document.getElementById("taskList")

//Obtenemos las tareas
const getData = async () => {
    const result = await list();
    if(!result.status) return alert("Something went wrong! - " + result.message);

    taskList.innerHTML = "";

    let arreglo = result.data;

   arreglo.map(
    (tarea) => 
        (taskList.innerHTML += TaskItem(tarea.id, tarea.title, tarea.description))
    );
};

getData();

//Eliminamos las tareas
window.deleteData = async (id) => {
    const respuesta = confirm("Do you want to delete this task?");
    if(!respuesta) return;

    const result = await deleteData(id);
    if(!result.status) return alert("Something went wrong! - " + result.message);

    getData();
};