import { get, create, update } from "../services/task.services.js";

//Obteniendo dom elemnts
const form = document.getElementById("form");
const formTitle = document.getElementById("formTitle");
const txtTitle = document.getElementById("txtTitle");
const txtDescripction = document.getElementById("txtDescription");
const btnForm = document.getElementById("btnForm");

//Obtener el id
const params = () => {
    const url = window.location.href;
    const param = url.split("#")[1];
    return param;
};

//Obtenemos task
const getTask = async () => {
    const id = params();

    if (!id) {
        formTitle.innerHTML = "Save task";
        btnForm.innerHTML = "Save";
        return;
    }

    formTitle.innerHTML = "Update task";
    btnForm.innerHTML = "Update";

    const result = await get(id);
    if(!result.status) return alert("Something went wrong! - " + result.message);

    txtTitle.value = result.data.title;
    txtDescripction.value = result.data.description;

};

getTask();

//Evento para enviar la info a la api

form.addEventListener("submit", async (e) => {
    e.preventDefault();

    const id = params();

    const task = {
        title: txtTitle.value,
        description: txtDescripction.value,
    };

    let result;

    if(id) {
        result = await update(id, task);
    } else {
        result = await create(task);
    }
    if(!result.status) return alert("Something went wrong! - " + result.message);

    window.location.pathname = "/";
});